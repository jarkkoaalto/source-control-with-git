# Sourse-Control-with-Git

Linux Academy Source Control with Git Repository

## Installation of git

###### Installation of git Centos7
	
	yum install git
	git --version

###### Installation of git Debian based OS
	
	sudo apt install git
	
## Basics of the Git

	git -> working directory storing area. You can create master branch and if needed you can create more branches.

	git = working directory storing area. You can create master branch and if needed you can create more branches.

###### Create a local repository 

	git init hi-there

###### Create new local repository using --bare command. This command can be used when large server projects are needed.

	git init --bare big-broject.git

###### remove .git projects

	rm -rf big-broject.git/

###### Using config command

	git config --global user.name "etu suku" 
	git config --global "etu.suku@email.com"
	git config --global core.editor "/usr/bin/vim"

	git config --list

	cat ~/.fitconfig => show global atributes

###### use different email address different repository

	cd fake-projects
	git config user.email "etu.suku@email.com"

###### config settings

	git config --list

###### add files to a git project

	git add README.md

###### git status view you staged an pystaged files 

	git status

###### view the output in shortened format

	git status -server

###### get more verbose output, including what was changed in a file

	git status -v

###### local documentation for the git status command
	
	man git-status

###### git rm removes a file from projects

	git rm
	git rm -f "file"

###### git commit opens text editor to prepare for a commit of files in the staging area

	git commit

###### bypasses the editor and performs a commit with the specified message

	git commit -m "Commit Message"

###### commit a modified file in the staging area

	git commit -a -m "Message"

###### local documentation for the commit commands for git

	man git-commit

	git tag -a <tag name> -m <message>


## Tags, Branching, Merging and Reverting

###### crete an annotated tag

	git tag 

###### view all tagas for the repository

	git tag <tag name> -m<message>

###### create a lightweight tagas

	git tag -d <tag name>

###### delete a specific tag

	man git-tag

######

git tag -a (tag name) -m (message)

###### crete an annotated tag

git tag 

###### view all tagas for the repository

git tag (tag name) -m (message)

###### create a lightweight tagas

git tag -d (tag name)

###### delete a specific tag

man git-tag

###### local documentation for the git tag command

###### original file that contains file pattern that git will not track
	
	.git/info/exclude

###### ignore file local to a git repository commonly used to exlude files nased on patterns

	.gitignore

###### used to debug git ignore to see what is and is not being excluded from git

	git check-ignore <pattern>

###### local documentation for the gitignore file

	man gitignore

###### Using Branches

###### git brance branch name create a new branch of the project tee

	git ranch branch name

###### git checkout branch name switches to another branch

	git checkout branch name

###### HEAD pointer to the current branch being worked on, can use git log and git status to view which branch HEAD is pointing to

	HEAD

###### man git-branch local documentation for the git branch command

	man git-branch

###### man git checkout local documentation for the git checkout command

	man git-checkout

	.git/info/exclude

###### ignore file local to a git repository commonly used to exlude files nased on patterns

	.gitignore

###### used to debug git ignore to see what is and is not being excluded from git	

	git check-ignore <pattern>

###### local documentation for the gitignore file

	man gitignore

###### Merging Branches

###### git merge combines the latest commits from two branches into one branch

	git merge

###### git branch-d (branch) deletes speciafied brranch

	git branch -d (branch)

###### man git-merge local documentation on using the git merge command

	man git-merge

##  Rebasing
###### bit rebase (branch) replays changes made to one branch over the top of another branch

	git rebase <branch>

###### man git-rebase local documentation on using the git rebase command

	git man-rebase

###### Reventing a Commit
###### git revert (commit) revert a commit in the project
	
	git revert (commit)

###### man git-revert local documentation on using the git revert command

	man git-revert

###### Using the diff commend viw the differience between two commits, files, blobs, ore between the working tree and the staging area

	git diff

###### man git-diff local documentation on using the git diff command
	
	man git-diff

###### How garbage collection works
###### git gc the git garbage collection command, cleans out old objects that can nott be referenced by the database anymore, abd compresses contents within the git directory to save disk space

	git gc

###### git gc-prune by default, cleans out objects that are older than two weeks
		
	git gc--prune

###### man git-gc local documentation on using the git gc command
	
	man git-gc

## Git's Logs and Auditing

###### Git log view the git repository's history

git log

###### git log --graph show a textural graph of a project's commit history

git log --graph

###### git log --stat show statistics of the files with each commit

git log --stat

###### git log --pretty=format: format the output of a git log command to dispaly specifig fields

git log --pretty=format:

###### Example %h specify format h=hash -%ar=relativetime %an=autohorsname %s=subjects

git log --pretty=format:"%h 

###### man git-log local documentation on using th git log command

man git-log

## Cloning Local Repository

###### git clone (local repository) (new Repository) clones a local repository to a nre repostitory on the local file system

git clone local repo new repo

###### man git-clone local documentation on using the git clone command

man git-clone

###### git clone remote URL clones a remote git repository to the local file system

git clone remoteURL

###### man git-clone local documentation on using the git clone command

## Pull Request: Push, Pull and tracking remote repositories

###### Tracking remote repository

###### git remote -v shows the remote servers that are being tracked for the cuurrent repository, and their latest statics

git remote -v

###### git fetch fetches new commit information down from the remote server for the current repostory, does not commit anything to local database

git fetch

###### man git-fetch local documentation on using the git fetch command

man git-fetch

###### man git-remote local documentation on using the git remote command

man git-remote

## Tracking Remote Repository

###### git push -u remote localbranch pushes local changes to the upstream git repostory

git push -u (remote) (local branch)

###### man git-push local documentation on using the git push command

man git-push

Be sure to have your SSH key set up and that your public key ( .ssh/id_rsa.pub) is added your repository.
